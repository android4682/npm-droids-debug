# About

When I first started to work with Node.JS I wanted a logging system that wasn't just white lines in the console.
*But just use some color codes*
Me remembering all the color codes would take me to long and I just wanted to code.

So that's why I started to work out my own simple logging system.
Nothing to special just 3 debug modes with 3 different colors and identifiers.

When I started doing Java I revisited my JS debug logging system.
And now I did it again but proper, in the form of an npm module.

# Dependencies

Best news, no dependencies!
It's all pure (Node.)JS!

# Installation, Usage and so

You can easily install this module via npm by opening your terminal and running:
```bash
npm i droid-debug
```

See the [Wiki](https://gitlab.com/android4682/npm-droids-debug/wikis/home) for more information.

# License

You can find the LICENSE in the file `LICENSE`.
Or TL;DR:
```
You can:
- Download
- Use
- Modify
- Publish your own versions

You can NOT:
- Publish your own version without crediting the original author
- Pretending to be the original author
```