'use strict'

const internal = {};
const _colorScheme = require("./colorScheme.js")

/**
 * Generate a random number between the 2 givin numbers
 * @param {Number} low **Lowest number it can return*
 * @param {Number} high **Highest number it can return*
 * @return {Number} **Returns a random number between the 2 givin numbers*
 */
function _randomInt(low, high) {
	return Math.floor(Math.random() * (high - low + 1) + low)
}

module.exports = internal.DroidsDebug = class {
	/**
	 * Construct DroidsDebug
	 * @param {(String|Number)} debugMode **Sets which debug mode to use*
	 */
	constructor(debugMode) {
		_debug = false
		_killerDebug = false
		_devDebug = false
		_debugMode = ""
		if (debugMode == "debug" || debugMode == 1) {
			_debug = true
			_debugMode = "DEBUG"
			console.log(_colorScheme.once.debug + `[DroidsDebug][${_debugMode}] Debug enabled!` + _colorScheme.once.reset)
		} else if (debugMode == "killme" || debugMode == 2) {
			_killerDebug = true
			_debugMode = "KillerDEBUG"
			const killerColor = {
				chat: "\x1b[41m\x1b[1m\x1b[37m",
				reset: "\x1b[0m\x1b[1m"
			}
			const killerMessages = {
				welcome: [
					`${killerColor.chat}I'm your worst nightmare.${killerColor.reset}`,
					`${killerColor.chat}I'm your worst debugger.${killerColor.reset}`,
					`${killerColor.chat}I hope I'll kill you with my verbose!${killerColor.reset}`
				]
			}
			console.log(_colorScheme.debug, `[DroidsDebug][${_debugMode}] Killer debug enabled!`)
			console.log(_colorScheme.once.killerDebug + `[DroidsDebug][${_debugMode}-Chat] ${killerMessages.welcome[_randomInt(0, killerMessages.welcome.length-1)]}` + _colorScheme.once.reset)
		} else if (debugMode == "devops" || debugMode == 3) {
			_devDebug = true
			_debugMode = "Dev-DEBUG"
			const devColor = {
				chat: "\x1b[0m\x1b[45m\x1b[1m\x1b[37m",
				reset: "\x1b[0m\x1b[1m"
			}
			const devMessages = {
				welcome: [
					`${devColor.chat}Every detail is visible!${devColor.reset}`,
					`${devColor.chat}This isn't gonna be fun..${devColor.reset}`,
					`${devColor.chat}Oh boy. Here we go again!${devColor.reset}`,
					`${devColor.chat}If I'm here that means...${devColor.reset}`,
					`${devColor.chat}OH SHIET!${devColor.reset}`
				]
			}
			console.log(_colorScheme.debug, `[DroidsDebug][${_debugMode}] Dev debug enabled!`)
			console.log(_colorScheme.once.devDebug + `[DroidsDebug][${_debugMode}-Chat] ${devMessages.welcome[_randomInt(0, devMessages.welcome.length-1)]}` + _colorScheme.once.reset)
		}
	}
	/**
	 * Send message to console however this function takes the variable `debugMode` that defines what type of debug the message is.
	 * Also accepts the variable `moduleStr` to customize the module message.
	 * @param {String} msg **Message to be logged to console*
	 * @param {Number} debugMode **Sets which debug mode the message is intended to*
	 * @param {String} moduleStr **Customize module part of the debug message*
	 * @param {(Array|Object)} dirVar **Array or Object that needs to be fully printed*
	 */
	log(msg, debugMode, moduleStr, dirVar) {
		if (moduleStr == null) {
			moduleStr = "[DroidsDebug]"
		} else {
			moduleStr = `[${moduleStr}]`
		}
		switch (debugMode) {
			case 1:
				if (_debug) {
					console.log(_colorScheme.once.debug + moduleStr + "[DEBUG]" + "[Unique] " + msg + _colorScheme.once.reset)
					if (dirVar != null) {
						console.dir(dirVar)
					}
					return true
				} else if (_killerDebug || _devDebug) {
					console.log(_colorScheme.once.debug + moduleStr + "[DEBUG] " + msg + _colorScheme.once.reset)
					if (dirVar != null) {
						console.dir(dirVar)
					}
					return true
				}
				return false
			case 2:
				if (_killerDebug) {
					console.log(_colorScheme.once.killerDebug + moduleStr + "[KillerDEBUG]" + "[Unique] " + msg + _colorScheme.once.reset)
					if (dirVar != null) {
						console.dir(dirVar)
					}
					return true
				} else if (_devDebug) {
					console.log(_colorScheme.once.killerDebug + moduleStr + "[KillerDEBUG] " + msg + _colorScheme.once.reset)
					if (dirVar != null) {
						console.dir(dirVar)
					}
					return true
				}
				return false
			case 3:
				if (_devDebug) {
					console.log(_colorScheme.once.devDebug + moduleStr + "[Dev-DEBUG]" + "[Unique] " + msg + _colorScheme.once.reset)
					if (dirVar != null) {
						console.dir(dirVar)
					}
					return true
				}
				return false
			default:
				console.log(_colorScheme.once.white + moduleStr + " " + msg + _colorScheme.once.reset)
				if (dirVar != null) {
					console.dir(dirVar)
				}
				return true
		}
	}
	/**
	 * Send message to console (only to be used for debug messages!)
	 * Deprecated. Please use log().
	 * @param {String} dMsg **Message to be logged to console*
	 * @param {Array,Object} dirVar **Array or object that needs to be fully printed*
	 */
	debugMsg(dMsg, dirVar) {
		if (_debug) {
			console.log(_colorScheme.once.debug + "[Unique] " + dMsg + _colorScheme.once.reset)
			if (dirVar != null) {
				console.dir(dirVar)
			}
			return true
		} else if (_killerDebug) {
			console.log(_colorScheme.once.killerDebug + dMsg + _colorScheme.once.reset)
			if (dirVar != null) {
				console.dir(dirVar)
			}
			return true
		} else if (_devDebug) {
			console.log(_colorScheme.once.devDebug + dMsg + _colorScheme.once.reset)
			if (dirVar != null) {
				console.dir(dirVar)
			}
			return true
		}
		return false
	}
	/**
	 * Send message to console (only to be used for killer debug messages!)
	 * Deprecated. Please use log().
	 * @param {String} dMsg **Message to be logged to console*
	 * @param {Array,Object} dirVar **Array or object that needs to be fully printed*
	 */
	kDebugMsg(dMsg, dirVar) {
		if (_killerDebug) {
			console.log(_colorScheme.once.killerDebug + "[Unique] " + dMsg + _colorScheme.once.reset)
			if (dirVar != null) {
				console.dir(dirVar)
			}
			return true
		} else if (_devDebug) {
			console.log(_colorScheme.once.devDebug + dMsg + _colorScheme.once.reset)
			if (dirVar != null) {
				console.dir(dirVar)
			}
			return true
		}
		return false
	}
	/**
	 * Send message to console (only to be used for dev debug messages!)
	 * Deprecated. Please use log().
	 * @param {String} dMsg **Message to be logged to console*
	 * @param {Array,Object} dirVar **Array or object that needs to be fully printed*
	 */
	devMsg(dMsg, dirVar) {
		if (_devDebug) {
			console.log(_colorScheme.once.devDebug + "[Unique] " + dMsg + _colorScheme.once.reset)
			if (dirVar != null) {
				console.dir(dirVar)
			}
			return true
		}
		return false
	}
	/**
	 * Get debug boolean.
	 * Also see enableDebug()
	 * @returns {Boolean} **Whenever debug mode is enabled*
	 */
	getDebug() {
		return _debug
	}
	/**
	 * Get killer debug boolean.
	 * Also see enableKillerDebug()
	 * @returns {Boolean} **Whenever killer debug mode is enabled*
	 */
	getKillerDebug() {
		return _killerDebug
	}
	/**
	 * Get dev debug boolean.
	 * Also see enableDevDebug()
	 * @returns {Boolean} **Whenever dev debug mode is enabled*
	 */
	getDevDebug() {
		return _devDebug
	}
	/**
	 * Get the string that is indicating which kind of logging is enabled.
	 * @returns {String} **Type of logging string*
	 */
	getDebugMode() {
		return _debugMode
	}
	/**
	 * Sets debug variable.
	 * Also see enableDebug()
	 * @param {Boolean} newVal **If debug should be enabled or not*
	 */
	setDebug(newVal) {
		_debug = newVal
	}
	/**
	 * Sets killer debug variable.
	 * Also see enableKillerDebug()
	 * @param {Boolean} newVal **If killer debug should be enabled or not*
	 */
	setKillerDebug(newVal) {
		_killerDebug = newVal
	}
	/**
	 * Sets dev debug variable.
	 * Also see enableDevDebug()
	 * @param {Boolean} newVal **If dev debug should be enabled or not*
	 */
	setDevDebug(newVal) {
		_devDebug = newVal
	}
	/**
	 * Sets string that indicated which debug mode is being used.
	 * @param {Boolean} newVal **New indication of which debug mode is being used*
	 */
	setDebugMode(newVal) {
		_debugMode = newVal
	}
	/**
	 * Enables debug mode and disable the others.
	 */
	enableDebug() {
		_debug = true
		_killerDebug = false
		_devDebug = false
		_debugMode = "DEBUG"
	}
	/**
	 * Enables killer debug mode and disable the others.
	 */
	enableKillerDebug() {
		_debug = false
		_killerDebug = true
		_devDebug = false
		_debugMode = "KillerDEBUG"
	}
	/**
	 * Enables dev debug mode and disable the others.
	 */
	enableDevDebug() {
		_debug = false
		_killerDebug = false
		_devDebug = true
		_debugMode = "Dev-DEBUG"
	}
}

// Defining debug just in case
let _debug = false
let _killerDebug = false
let _devDebug = false
let _debugMode = "NO-DEBUG"
