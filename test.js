Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

const DroidsDebug = require("./index.js")
const colorScheme = require("./colorScheme.js")

console.log("\x1b[1m")

var results = {
	"debugTest": {
		success: null,
		fired: 0,
		lines: 0
	},
	"killerDebugTest": {
		success: null,
		fired: 0,
		lines: 0
	},
	"devDebugTest": {
		success: null,
		fired: 0,
		lines: 0
	},
	"gettersTest": {
		success: null,
		getDebug: null,
		getKillerDebug: null,
		getDevDebug: null,
		getDebugMode: null
	},
	"settersTest": {
		success: null,
		getDebug: null,
		getKillerDebug: null,
		getDevDebug: null,
		getDebugMode: null
	},
	"enablesTest": {
		success: null,
		getDebug1: null,
		getKillerDebug1: null,
		getDevDebug1: null,
		getDebugMode1: null,
		getDebug2: null,
		getKillerDebug2: null,
		getDevDebug2: null,
		getDebugMode2: null,
		getDebug3: null,
		getKillerDebug3: null,
		getDevDebug3: null,
		getDebugMode3: null
	}
}

const expected = {
	"debugTest": {
		fired: 7,
		lines: 3
		//maxLines: 7
	},
	"killerDebugTest": {
		fired: 7,
		lines: 6
		//maxLines: 8
	},
	"devDebugTest": {
		fired: 7,
		lines: 8
		//maxLines: 8
	},
	"gettersTest": {
		getDebug: true,
		getKillerDebug: false,
		getDevDebug: false,
		getDebugMode: "DEBUG"
	},
	"settersTest": {
		getDebug: false,
		getKillerDebug: true,
		getDevDebug: true,
		getDebugMode: "TEST-DEBUG"
	},
	"enablesTest": {
		getDebug1: true,
		getKillerDebug1: false,
		getDevDebug1: false,
		getDebugMode1: "DEBUG",
		getDebug2: false,
		getKillerDebug2: true,
		getDevDebug2: false,
		getDebugMode2: "KillerDEBUG",
		getDebug3: false,
		getKillerDebug3: false,
		getDevDebug3: true,
		getDebugMode3: "Dev-DEBUG"
	}
}

var debugTestRes = null
var killerDebugTestRes = null
var devDebugTestRes = null
var gettersTestRes = null

function debugTest() {
	/***
	 * Expecting *
	 * Fired: 7
	 * Lines: 3
	 * maxLines: 7
	 */
	console.log("\nTesting debug mode...\n")

	const debug = new DroidsDebug("debug")
	let Lines = 1
	let Fired = 1
	if (debug.debugMsg("debugMsg check in debug mode, finished.")) Lines++;
	Fired++
	if (debug.kDebugMsg("kDebugMsg check in debug mode, you are NOT supposed to see this.")) Lines++;
	Fired++
	if (debug.devMsg("devMsg check in debug mode, you are NOT supposed to see this.")) Lines++;
	Fired++

	if (debug.log("send-1 check in debug mode, finished.", 1)) Lines++;
	Fired++
	if (debug.log("send-2 check in debug mode, you are NOT supposed to see this.", 2)) Lines++;
	Fired++
	if (debug.log("send-3 check in debug mode, you are NOT supposed to see this.", 3)) Lines++;
	Fired++

	console.log(`\nDebug test done!`)
	results.debugTest.fired = Fired
	results.debugTest.lines = Lines
}

function killerDebugTest() {
	/***
	 * Expecting *
	 * Fired: 7
	 * Lines: 6
	 * maxLines: 8
	 */
	console.log("\nTesting killerDebug mode...\n")

	const killerDebug = new DroidsDebug("killme")
	let Lines = 2
	let Fired = 1
	if (killerDebug.debugMsg("debugMsg check in killerDebug mode, finished.")) Lines++;
	Fired++
	if (killerDebug.kDebugMsg("kDebugMsg check in killerDebug mode, finished.")) Lines++;
	Fired++
	if (killerDebug.devMsg("devMsg check in killerDebug mode, you are NOT supposed to see this.")) Lines++;
	Fired++

	if (killerDebug.log("send-1 check in killerDebug mode, finished.", 1)) Lines++;
	Fired++
	if (killerDebug.log("send-2 check in killerDebug mode, finished.", 2)) Lines++;
	Fired++
	if (killerDebug.log("send-3 check in killerDebug mode, you are NOT supposed to see this.", 3)) Lines++;
	Fired++

	console.log(`\nKiller Debug test done!`)
	results.killerDebugTest.fired = Fired
	results.killerDebugTest.lines = Lines
}

function devDebugTest() {
	/***
	 * Expecting *
	 * Fired: 7
	 * Lines: 8
	 * maxLines: 8
	 */
	console.log("\nTesting devDebug mode...\n")

	const devDebug = new DroidsDebug("devops")
	let Lines = 2
	let Fired = 1
	if (devDebug.debugMsg("debugMsg check in devDebug mode, finished.")) Lines++;
	Fired++
	if (devDebug.kDebugMsg("kDebugMsg check in devDebug mode, finished.")) Lines++;
	Fired++
	if (devDebug.devMsg("devMsg check in devDebug mode, finished.")) Lines++;
	Fired++

	if (devDebug.log("send-1 check in devDebug mode, finished.", 1)) Lines++;
	Fired++
	if (devDebug.log("send-2 check in devDebug mode, finished.", 2)) Lines++;
	Fired++
	if (devDebug.log("send-3 check in devDebug mode, finished.", 3)) Lines++;
	Fired++

	console.log(`\nDev Debug test done!`)
	results.devDebugTest.fired = Fired
	results.devDebugTest.lines = Lines
}

function gettersTest() {
	console.log("\nTesting getter functions...\n")
	const gettersDebug = new DroidsDebug("debug")
	results.gettersTest.getDebug = gettersDebug.getDebug()
	results.gettersTest.getKillerDebug = gettersDebug.getKillerDebug()
	results.gettersTest.getDevDebug = gettersDebug.getDevDebug()
	results.gettersTest.getDebugMode = gettersDebug.getDebugMode()
	console.log(`\nGetters test done!`)
}

function settersTest() {
	console.log("\nTesting setters functions...\n")
	const settersDebug = new DroidsDebug("debug")
	settersDebug.setDebug(false)
	results.settersTest.getDebug = settersDebug.getDebug()
	settersDebug.setKillerDebug(true)
	results.settersTest.getKillerDebug = settersDebug.getKillerDebug()
	settersDebug.setDevDebug(true)
	results.settersTest.getDevDebug = settersDebug.getDevDebug()
	settersDebug.setDebugMode("TEST-DEBUG")
	results.settersTest.getDebugMode = settersDebug.getDebugMode()
	console.log(`\nSetters test done!`)
}

function enablesTest() {
	console.log("\nTesting enable functions...\n")
	const enablesDebug = new DroidsDebug("devops")
	enablesDebug.enableDebug()
	results.enablesTest.getDebug1 = enablesDebug.getDebug()
	results.enablesTest.getKillerDebug1 = enablesDebug.getKillerDebug()
	results.enablesTest.getDevDebug1 = enablesDebug.getDevDebug()
	results.enablesTest.getDebugMode1 = enablesDebug.getDebugMode()
	enablesDebug.enableKillerDebug()
	results.enablesTest.getDebug2 = enablesDebug.getDebug()
	results.enablesTest.getKillerDebug2 = enablesDebug.getKillerDebug()
	results.enablesTest.getDevDebug2 = enablesDebug.getDevDebug()
	results.enablesTest.getDebugMode2 = enablesDebug.getDebugMode()
	enablesDebug.enableDevDebug()
	results.enablesTest.getDebug3 = enablesDebug.getDebug()
	results.enablesTest.getKillerDebug3 = enablesDebug.getKillerDebug()
	results.enablesTest.getDevDebug3 = enablesDebug.getDevDebug()
	results.enablesTest.getDebugMode3 = enablesDebug.getDebugMode()
	console.log(`\nEnable functions test done!`)
}

function report(subject) {
	switch (subject) {
		case "debug":
			var subjectArr = "debugTest"
			break;
		case "killerDebug":
			var subjectArr = "killerDebugTest"
			break;
		case "devDebug":
			var subjectArr = "devDebugTest"
			break;
		case "getters":
			var subjectArr = "gettersTest"
			break;
		case "setters":
			var subjectArr = "settersTest"
			break;
		case "enables":
			var subjectArr = "enablesTest"
			break;
		case "total":
			let testRan = 0
			let testSuccess = 0
			let testFailed = 0
			for (let key in results) {
				switch (results[key].success) {
					case true:
						testRan++
						testSuccess++
						break;
					case false:
						testRan++
						testFailed++
						break;
					default:
						break;
				}
			}
			console.log(colorScheme.blue, `\nTESTS RAN: ${testRan} out of ${Object.size(expected)}`)
			console.log(colorScheme.green, `SUCCESSFUL: ${testSuccess}`)
			console.log(colorScheme.red, `FAILED: ${testFailed}\n`)
			return;
		default:
			report("debug")
			report("killerDebug")
			report("devDebug")
			report("getters")
			report("setters")
			report("enables")
			report("total")
			return;
	}
	console.log(`\n${subject} test results:`)
	for (let keyResult in expected[subjectArr]) {
		let color = colorScheme.purple
		if (results[subjectArr][keyResult] == expected[subjectArr][keyResult]) {
			color = colorScheme.green
		} else {
			color = colorScheme.red
		}
		console.log(color, `${keyResult}: ${results[subjectArr][keyResult]} (expected: ${expected[subjectArr][keyResult]})`)
	}
	let correctResults = 0
	for (let keyResult in expected[subjectArr]) {
		if (results[subjectArr][keyResult] == expected[subjectArr][keyResult]) {
			correctResults++
		}
	}
	if (correctResults == Object.size(expected[subjectArr])) {
		console.log(colorScheme.green, "\nTest: Successful")
		results[subjectArr].success = true
	} else {
		console.log(colorScheme.red, "\nTest: Failed")
		results[subjectArr].success = false
	}
}

if (process.argv.indexOf("debug") != -1) {
	debugTest()
	report("debug")
	report("total")
} else if (process.argv.indexOf("killerDebug") != -1) {
	killerDebugTest()
	report("killerDebug")
	report("total")
} else if (process.argv.indexOf("devDebug") != -1) {
	devDebugTest()
	report("devDebug")
	report("total")
} else if (process.argv.indexOf("getters") != -1) {
	gettersTest()
	report("getters")
	report("total")
} else if (process.argv.indexOf("setters") != -1) {
	settersTest()
	report("setters")
	report("total")
} else if (process.argv.indexOf("enables") != -1) {
	enablesTest()
	report("enables")
	report("total")
} else {
	debugTest()
	killerDebugTest()
	devDebugTest()
	gettersTest()
	settersTest()
	enablesTest()
	report()
}

process.exit(0)
