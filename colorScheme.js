module.exports = {
	brighter: "\x1b[1m",
	darker: "\x1b[2m",
	reset: "\x1b[0m\x1b[1m",
	error: "\x1b[31m%s\x1b[0m\x1b[1m", // Red
	warning: "\x1b[33m%s\x1b[0m\x1b[1m", // Yellow
	critical: "\x1b[0m\x1b[2m\x1b[33m%s\x1b[0m\x1b[1m", // Gold
	connectionIS: "\x1b[36m%s\x1b[0m\x1b[1m", // Cyan
	info: "\x1b[34m%s\x1b[0m\x1b[1m", // Blue
	execSuccess: "\x1b[32m%s\x1b[0m\x1b[1m", // Green
	debug: "\x1b[35m%s\x1b[0m\x1b[1m", // Purple
	devDebug: "\x1b[0m\x1b[35m%s\x1b[1m", // Dark Purple
	killerDebug: "\x1b[0m\x1b[2m\x1b[31m%s\x1b[1m", // darkRed
	white: "\x1b[37m%s\x1b[0m\x1b[1m", // White, duh.
	red: "\x1b[31m%s\x1b[0m\x1b[1m", // error
	darkRed: "\x1b[0m\x1b[2m\x1b[31m%s\x1b[1m", // killerDEBUG!
	yellow: "\x1b[33m%s\x1b[0m\x1b[1m", // warning
	gold: "\x1b[0m\x1b[2m\x1b[33m%s\x1b[0m\x1b[1m", // critical
	cyan: "\x1b[36m%s\x1b[0m\x1b[1m", // connection is succesfull
	blue: "\x1b[34m%s\x1b[0m\x1b[1m", // info
	green: "\x1b[32m%s\x1b[0m\x1b[1m", // execution success
    purple: "\x1b[35m%s\x1b[0m\x1b[1m", // debug
    darkPurple: "\x1b[0m\x1b[35m%s\x1b[1m", // devDebug
    gray: "\x1b[2m\x1b[37m%s\x1b[0m\x1b[1m",
    once: {
        brighter: "\x1b[1m",
        darker: "\x1b[2m",
        reset: "\x1b[0m\x1b[1m",
        error: "\x1b[31m", // Red
        warning: "\x1b[33m", // Yellow
        critical: "\x1b[0m\x1b[2m\x1b[33m", // Gold
        connectionIS: "\x1b[36m", // Cyan
        info: "\x1b[34m", // Blue
        execSuccess: "\x1b[32m", // Green
        debug: "\x1b[35m", // Purple
        devDebug: "\x1b[0m\x1b[35m", // Dark Purple
        killerDebug: "\x1b[0m\x1b[2m\x1b[31m", // darkRed
        white: "\x1b[37m", // White, duh.
        red: "\x1b[31m", // error
        darkRed: "\x1b[0m\x1b[2m\x1b[31m", // killerDEBUG!
        yellow: "\x1b[33m", // warning
        gold: "\x1b[0m\x1b[2m\x1b[33m", // critical
        cyan: "\x1b[36m", // connection is succesfull
        blue: "\x1b[34m", // info
        green: "\x1b[32m", // execution success
        purple: "\x1b[35m", // debug
        darkPurple: "\x1b[0m\x1b[35m", // devDebug
        gray: "\x1b[2m\x1b[37m",
    }
}